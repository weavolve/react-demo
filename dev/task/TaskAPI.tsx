import AxiosInstance from "../../utils/AxiosInstance";
import { TaskDetailInterface } from "./models/TaskDetailInterface";

const API_ROUTE = "tasks"
export const getTaskList = async () => {
    return await AxiosInstance.get(`/${API_ROUTE}`)
}

export const getTaskDetail = async (index : string) => {
  return await AxiosInstance.get(`/${API_ROUTE}/${index}`)
}

export const createTask = async (data : TaskDetailInterface) => {
  return await AxiosInstance.post(`/${API_ROUTE}`, data)
}

export const deleteTask = async (index : string) => {
  return await AxiosInstance.delete(`/${API_ROUTE}/${index}`)
}
export const updateTask = async (index : string, data : TaskDetailInterface) => {
  return await AxiosInstance.put(`/${API_ROUTE}/${index}`, data)
}