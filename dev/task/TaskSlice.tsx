import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { RootState } from '../../stores';
import { TaskDetailInterface } from './models/TaskDetailInterface';
import { createTask, deleteTask, getTaskDetail, getTaskList, updateTask } from './TaskAPI';

export interface TaskStateInterface {
  tasks: TaskDetailInterface[];
  favourites: TaskDetailInterface[];
  totalRecord: number;
  status: 'idle' | 'loading' | 'failed' | 'created' | 'updated' | 'deleted';
  detail: TaskDetailInterface
}

const initialState: TaskStateInterface = {
  tasks: [],
  favourites: [],
  totalRecord: 0,
  status: 'idle',
  detail: {
    id: "",
    title: "",
    description: ""
  }
};

export const getTaskListAsync = createAsyncThunk(
  'task/list',
  async () => {
    const response = await getTaskList();
    return response.data;
  }
);
export const getTaskDetailAsync = createAsyncThunk(
  'task/detail',
  async (index: string) => {
    const response = await getTaskDetail(index);
    return response.data;
  }
);

export const createTaskAsync = createAsyncThunk(
  'task/create',
  async (data: TaskDetailInterface) => {
    const response = await createTask(data);
    return response.data;
  }
);

export const deleteTaskAsync = createAsyncThunk(
  'task/delete',
  async (index: string) => {
    const response = await deleteTask(index);
    return response.data;
  }
);

export const updateTaskAsync = createAsyncThunk(
  'task/update',
  async (data: {
    index: string, data: TaskDetailInterface
  }) => {
    const response = await updateTask(data.index, data.data);
    return response.data;
  }
);

export const taskSlice = createSlice({
  name: 'task',
  initialState,
  reducers: {
    
  },
  extraReducers: (builder) => {
    builder
      // GET LIST
      .addCase(getTaskListAsync.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(getTaskListAsync.fulfilled, (state, action) => {
        state.status = 'idle';
        state.tasks = action.payload.data;
        state.totalRecord = action.payload.total
      })
      .addCase(getTaskListAsync.rejected, (state) => {
        state.status = 'failed';
      })
      // GET DETAIL
      .addCase(getTaskDetailAsync.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(getTaskDetailAsync.fulfilled, (state, action) => {
        state.status = 'idle';
        state.detail = action.payload;
      })
      .addCase(getTaskDetailAsync.rejected, (state) => {
        state.status = 'failed';
      })
      // CREATE
      .addCase(createTaskAsync.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(createTaskAsync.fulfilled, (state, action) => {
        state.status = 'created';
        state.detail = action.payload;
      })
      .addCase(createTaskAsync.rejected, (state) => {
        state.status = 'failed';
      })
      // UPDATE
      .addCase(updateTaskAsync.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(updateTaskAsync.fulfilled, (state, action) => {
        state.status = 'updated';
        state.detail = action.payload;
      })
      .addCase(updateTaskAsync.rejected, (state) => {
        state.status = 'failed';
      })
      // DELETE
      .addCase(deleteTaskAsync.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(deleteTaskAsync.fulfilled, (state, action) => {
        state.status = 'deleted';
        state.detail = action.payload;
      })
      .addCase(deleteTaskAsync.rejected, (state) => {
        state.status = 'failed';
      });
  },
});

export const {  } = taskSlice.actions;
export const TaskState = (state: RootState) => state.task;
export default taskSlice.reducer;
