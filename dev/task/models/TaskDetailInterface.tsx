export interface TaskDetailInterface {
    id?: string,
    title?: string
    description: string
}