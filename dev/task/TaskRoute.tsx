import { Route } from "react-router-dom";
import TaskCreate from "./pages/TaskCreate";
import TaskList from "./pages/TaskList";

/**
 * This will return all routes for the specific module
 */
const MODULE_ROUTE = "task"; 
const TaskRoute = [
    <Route path={`${MODULE_ROUTE}`} element={<TaskList />} key={`${MODULE_ROUTE}_index`} />,
    <Route path={`${MODULE_ROUTE}/create`} element={<TaskCreate />}  key={`${MODULE_ROUTE}_create`}  />,
    <Route path={`${MODULE_ROUTE}/update/:id`} element={<TaskCreate />} key={`${MODULE_ROUTE}_edit`} />,
  ];

export default TaskRoute;