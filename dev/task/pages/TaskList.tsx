import { useEffect } from "react";
import { Alert, Button, Col, Row, Spinner, } from "react-bootstrap"
import { Link } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../../stores/hooks";
import { getTaskListAsync,  TaskState } from "../TaskSlice";
import { FaEdit, FaTrash } from "react-icons/fa";
import { TaskDetailInterface } from "../models/TaskDetailInterface";
import ModuleConfig from "./../config/ModuleConfig";
/**
 * Tasking list page
 */
const TaskList = () => {

    const { tasks, status, favourites } = useAppSelector(TaskState);
    const dispatch = useAppDispatch();
    useEffect(() => {
        dispatch(getTaskListAsync());
    }, [dispatch])
    return (
        <>

            <div className="page-header">
                <div className="page-block">
                    <div className="row align-items-center">
                        <div className="col-md-12">
                            <div className="page-header-title">
                                <h5 className="m-b-10">Task</h5>
                            </div>
                            <ul className="breadcrumb">
                                <li className="breadcrumb-item"><Link to="/"><i className="feather icon-home"></i></Link></li>
                                <li className="breadcrumb-item"><a >Task List</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div className="main-body">
                <div className="page-wrapper">
                    <div className="row">
                        <div className="col-xl-12">
                            <div className="card">
                                <div className="card-header">
                                    <Row>
                                        <Col auto><h5>Task List</h5></Col>
                                        <Col md={2}><Link className="btn btn-primary" to="/task/create">Add New Task</Link></Col>
                                    </Row>

                                </div>
                                <div className="card-block table-border-style">
                                    {status === "loading" ?
                                        <Spinner animation="border" role="status">
                                            <span className="visually-hidden">Loading...</span>
                                        </Spinner>
                                        : status === "failed" ?
                                            <Alert key={"danger"} variant={"danger"}>
                                                Somthing went wrong please try again
                                            </Alert>
                                            :
                                            <div className="table-responsive">
                                                <table className="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            {ModuleConfig.listColumnOrder.map((column: string, index: number) => {
                                                                return <th key={`head_${index}`}>{ModuleConfig.formFields[column].label}</th>

                                                            })}
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        {tasks?.length >= 1 ? tasks.map((task: TaskDetailInterface, index: number) => {
                                                            return (
                                                                <tr key={`table_row_${index}`}>
                                                                    <>
                                                                        {ModuleConfig.listColumnOrder.map((column: string, colIndex: number) => {
                                                                            return (<td key={`table_row_${index}_${colIndex}`}>{task[column]}</td>)
                                                                        })}

                                                                        <td>
                                                                            <Link to={`/task/update/${task[ModuleConfig.primaryKey]}`}><FaEdit /></Link>
                                                                            <Link to={``}><FaTrash /></Link>
                                                                            
                                                                        </td>
                                                                    </>
                                                                </tr>
                                                            )
                                                        }) : <tr>
                                                            <td colSpan={ModuleConfig.listColumnOrder.length + 1}>No Record.</td>
                                                        </tr>}

                                                    </tbody>
                                                </table>
                                            </div>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default TaskList;