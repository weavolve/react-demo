const fs = require("fs");
var path = require('path');
const { exit } = require("process");
const DEFAULT_NAME = "Task";
const NEW_MODULE_NAME = process.argv[2];
const MODULE_LOCATION = "../src/modules/";
const ROUTE_FILE = "../src/routes/index.tsx";
const STORE_FILE = "../src/stores/index.tsx";
const MENU_FILE = "../src/modules/common/menu.json";



/**
 * Look ma, it's cp -R.
 * @param {string} src  The path to the thing to copy.
 * @param {string} dest The path to the new copy.
 */
var copyRecursiveSync = function (src, dest) {
  var exists = fs.existsSync(src);
  var stats = exists && fs.statSync(src);
  var isDirectory = exists && stats.isDirectory();
  if (isDirectory) {
    fs.mkdirSync(dest);
    fs.readdirSync(src).forEach(function (childItemName) {

      const NewFileName = childItemName.replace(DEFAULT_NAME, NEW_MODULE_NAME)
      // childItemName = 
      copyRecursiveSync(path.join(src, childItemName),
        path.join(dest, NewFileName));
    });
  } else {
    fs.copyFileSync(src, dest);
  }
};

copyRecursiveSync(DEFAULT_NAME.toLocaleLowerCase(), MODULE_LOCATION + NEW_MODULE_NAME.toLocaleLowerCase());




var updateFileContent = function (src) {
  var exists = fs.existsSync(src);
  var stats = exists && fs.statSync(src);
  var isDirectory = exists && stats.isDirectory();
  if (isDirectory) {
    fs.readdirSync(src).forEach(function (childItemName) {
      updateFileContent(path.join(src, childItemName));
    });
  } else {
    updateContent(src);
  }
};

const updateContent = function (file) {
  fs.readFile(file, 'utf8', function (err, data) {
    const reg = new RegExp(DEFAULT_NAME, 'g')
    var result = data.replace(reg, NEW_MODULE_NAME);
    const reg1 = new RegExp(DEFAULT_NAME.toLocaleLowerCase(), 'g')
    result = result.replace(reg1, NEW_MODULE_NAME.toLocaleLowerCase());
    result = result.replace('const MODULE_ROUTE = "";', `const MODULE_ROUTE = "${NEW_MODULE_NAME.toLocaleLowerCase()}";`);
    fs.writeFile(file, result, 'utf8', function (err) {
      if (err) return console.log(err);
      console
    });
  });
}

updateFileContent(MODULE_LOCATION + NEW_MODULE_NAME.toLocaleLowerCase())



// UPDATE Route

fs.readFile(ROUTE_FILE, 'utf8', function (err, data) {
  const importLine = `import ${NEW_MODULE_NAME}Route from "../modules/${NEW_MODULE_NAME.toLocaleLowerCase()}/${NEW_MODULE_NAME}Route";`
  var result = data.replace(`{/* Import New Entry */ }`, `${importLine}\n{/* Import New Entry */ }`);
  var result = result.replace(`{/* Add New Entry */}`, `{${NEW_MODULE_NAME}Route}\n{/* Add New Entry */}`);
  fs.writeFile(ROUTE_FILE, result, 'utf8', function (err) {
    if (err) return console.log(err);
    console
  });
});


fs.readFile(STORE_FILE, 'utf8', function (err, data) {
  const importLine = `import ${NEW_MODULE_NAME.toLocaleLowerCase()}Reducer from "../modules/${NEW_MODULE_NAME.toLocaleLowerCase()}/${NEW_MODULE_NAME}Slice";`
  var result = data.replace(`/* import store */`, `${importLine}\n/* import store */`);
  var result = result.replace(`// {addStore}`, `${NEW_MODULE_NAME.toLocaleLowerCase()} : ${NEW_MODULE_NAME.toLocaleLowerCase()}Reducer,\n// {addStore}`);
  fs.writeFile(STORE_FILE, result, 'utf8', function (err) {
    if (err) return console.log(err);
    console
  });
});



fs.readFile(MENU_FILE, 'utf8', function (err, data) {

  const result = JSON.parse(data);
  result.push({
    "title": NEW_MODULE_NAME,
    "link": `/${NEW_MODULE_NAME.toLocaleLowerCase()}`,
    "icon": "icon-home"
  })

  fs.writeFile(MENU_FILE, JSON.stringify(result), 'utf8', function (err) {
   console.log(err);
  });
});