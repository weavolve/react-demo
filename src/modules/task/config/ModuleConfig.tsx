const ModuleConfig = {
    "primaryKey": "id",
    "formDefaultValue" : {
        "title" : "",
        "description" : ""
    },
    "formFields": {
        "title": {
            "label": "Title",
            "isRequired": true,
            "colWidth": "col-md-6",
            "inputType": "text",
            "type" :"text"
        },
        "description": {
            "label": "Description",
            "isRequired": true,
            "colWidth": "col-md-12",
            "inputType": "textarea",
            "type" :"text"
        }
    },
    "listColumnOrder": [
        "title",
        "description"
    ]
}

export default ModuleConfig