import { Button, Container, Row, Table } from "react-bootstrap"
import { Link, useNavigate, useParams } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../../stores/hooks";
import { createTaskAsync, getTaskDetailAsync, TaskState, updateTaskAsync } from "../TaskSlice";
import { Formik } from 'formik';
import { TaskDetailInterface } from "../models/TaskDetailInterface";
import ModuleConfig from "../config/ModuleConfig";
import FormField from "../../common/FormField";
import { useEffect } from "react";


/**
 * Tasking Favourites list page
 */
const TaskCreate = () => {
    const dispatch = useAppDispatch();
    const { status, detail } = useAppSelector(TaskState);
    const params = useParams();
    const navigator = useNavigate();

    useEffect(() => {
        if (params.id) {
            dispatch(getTaskDetailAsync(params.id));
        }
    }, [params, dispatch])

    useEffect(() => {
        if (status === "created") {
            navigator("/task")
        } else if (status === "updated") {
            navigator("/task")
        }
    }, [status])
    const handleSubmit = (values: TaskDetailInterface) => {
        if (params.id) {
            dispatch(updateTaskAsync({
                index: params.id,
                data: values
            }))
        } else {
            dispatch(createTaskAsync(values))
        }
    }


    return (
        <>
            <div className="page-header">
                <div className="page-block">
                    <div className="row align-items-center">
                        <div className="col-md-12">
                            <div className="page-header-title">
                                <h5 className="m-b-10">Task</h5>
                            </div>
                            <ul className="breadcrumb">
                                <li className="breadcrumb-item"><Link to="/"><i className="feather icon-home"></i></Link></li>
                                <li className="breadcrumb-item"><Link to="/task" >Task List</Link></li>
                                <li className="breadcrumb-item"><a>{params.id ? "Edit " : "Create "}Task</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div className="row">
                <div className="col-sm-12">
                    <div className="card">
                        <div className="card-header">
                            <h5>{params.id ? "Edit " : "Create "}Task</h5>
                        </div>
                        <div className="card-body">
                            <div className="row">
                                <div className="col-md-12">
                                    <Formik
                                        initialValues={detail[ModuleConfig.primaryKey] === params.id ? detail : ModuleConfig.formDefaultValue}
                                        validate={values => {
                                            const errors: any = {};
                                            const fields = Object.keys(ModuleConfig.formFields);
                                            for (let index = 0; index < fields.length; index++) {
                                                if (!values[fields[index]] && ModuleConfig.formFields[fields[index]].isRequired) {
                                                    errors[fields[index]] = 'Required';
                                                }
                                            }

                                            return errors;
                                        }}
                                        enableReinitialize={true}
                                        onSubmit={handleSubmit}
                                    >
                                        {({
                                            values,
                                            errors,
                                            touched,
                                            handleChange,
                                            handleBlur,
                                            handleSubmit,
                                            isSubmitting,
                                        }) => (
                                            <form onSubmit={handleSubmit}>

                                                <div className="row">
                                                    {Object.keys(ModuleConfig.formFields).map((field: string, index: number) => {
                                                        const fieldDetails = ModuleConfig.formFields[field]
                                                        return (
                                                            <div key={`field_${index}`} className={fieldDetails.colWidth}>
                                                                <FormField
                                                                    name={field}
                                                                    fieldDetails={fieldDetails}
                                                                    values={values}
                                                                    errors={errors}
                                                                    touched={touched}
                                                                    handleChange={handleChange}
                                                                    handleBlur={handleBlur}
                                                                />
                                                            </div>
                                                        )
                                                    })}


                                                </div>
                                                <hr />
                                                <button type="submit" className="btn btn-primary" disabled={status === "loading"}>
                                                    Submit
                                                </button>
                                            </form>)}
                                    </Formik>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default TaskCreate;