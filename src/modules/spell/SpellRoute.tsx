import { Route } from "react-router-dom";
import SpellDetail from "./pages/SpellDetail";
import SpellCreate from "./pages/SpellCreate";
import SpellList from "./pages/SpellList";

/**
 * This will return all routes for the specific module
 */
const MODULE_ROUTE = "spell"; 
const SpellRoute = [
    <Route path={`${MODULE_ROUTE}`} element={<SpellList />} key={`${MODULE_ROUTE}_index`} />,
    <Route path={`${MODULE_ROUTE}/detail/:id`} element={<SpellDetail />}  key={`${MODULE_ROUTE}_detail`}  />,
    <Route path={`${MODULE_ROUTE}/create`} element={<SpellCreate />}  key={`${MODULE_ROUTE}_create`}  />,
    <Route path={`${MODULE_ROUTE}/update/:id`} element={<SpellCreate />} key={`${MODULE_ROUTE}_edit`} />,
  ];

export default SpellRoute;