const ModuleConfig = {
    "primaryKey": "index",
    "formDefaultValue" : {
        "name" : "",
        "phone" : "",
        "address" : "",
        "password" : "",
        "cpassword" : "",
        "index" : "",
        "url" : "",
    },
    "formFields": {
        "name": {
            "label": "Spell Name",
            "isRequired": true,
            "colWidth": "col-md-6",
            "inputType": "text",
            "type" :"text"
        },
        "phone": {
            "label": "Phone",
            "isRequired": true,
            "colWidth": "col-md-6",
            "inputType": "text",
            "type" :"number"
        },
        "address": {
            "label": "Address",
            "isRequired": true,
            "colWidth": "col-md-12",
            "inputType": "textarea"
        },
        "password": {
            "label": "Password",
            "isRequired": true,
            "colWidth": "col-md-6",
            "inputType": "text",
            "type" :"password"
        },
        "cpassword": {
            "label": "Confrim Password",
            "isRequired": true,
            "colWidth": "col-md-6",
            "inputType": "text",
            "type" :"password"
        },
        "index": {
            "label": "Index",
            "isRequired": true,
            "colWidth": "col-md-6",
            "inputType": "text"
        },
        "url": {
            "label": "URL",
            "isRequired": false,
            "colWidth": "col-md-6",
            "inputType": "text"
        }
    },
    "listColumnOrder": [
        "name"
    ]
}

export default ModuleConfig