import { Button, Container, Row, Table } from "react-bootstrap"
import { Link, useParams } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../../stores/hooks";
import { createSpellAsync, getSpellDetailAsync, SpellState, updateSpellAsync } from "../SpellSlice";
import { Formik } from 'formik';
import { SpellDetailInterface } from "../models/SpellDetailInterface";
import ModuleConfig from "../config/ModuleConfig";
import FormField from "../../common/FormField";
import { useEffect } from "react";


/**
 * Spelling Favourites list page
 */
const SpellCreate = () => {
    const dispatch = useAppDispatch();
    const { status, detail } = useAppSelector(SpellState);
    const params = useParams();


    useEffect(() => {
        if (params.id) {
            dispatch(getSpellDetailAsync(params.id));
        }
    }, [params, dispatch])
    const handleSubmit = (values: SpellDetailInterface) => {
        if (params.id) {
            dispatch(updateSpellAsync({
                index: params.id,
                data: values
            }))
        } else {
            dispatch(createSpellAsync(values))
        }
    }


    return (
        <>
            <div className="page-header">
                <div className="page-block">
                    <div className="row align-items-center">
                        <div className="col-md-12">
                            <div className="page-header-title">
                                <h5 className="m-b-10">Spell</h5>
                            </div>
                            <ul className="breadcrumb">
                                <li className="breadcrumb-item"><Link to="/"><i className="feather icon-home"></i></Link></li>
                                <li className="breadcrumb-item"><Link to="/spell" >Spell List</Link></li>
                                <li className="breadcrumb-item"><a>{params.id ? "Edit " : "Create "}Spell</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div className="row">
                <div className="col-sm-12">
                    <div className="card">
                        <div className="card-header">
                            <h5>{params.id ? "Edit " : "Create "}Spell</h5>
                        </div>
                        <div className="card-body">
                            <div className="row">
                                <div className="col-md-12">
                                    <Formik
                                        initialValues={detail[ModuleConfig.primaryKey] === params.id ?  detail  : ModuleConfig.formDefaultValue}
                                        validate={values => {
                                            const errors: any = {};
                                            const fields = Object.keys(ModuleConfig.formFields);
                                            for (let index = 0; index < fields.length; index++) {
                                                if (!values[fields[index]] && ModuleConfig.formFields[fields[index]].isRequired) {
                                                    errors[fields[index]] = 'Required';
                                                }
                                            }
                                            
                                            return errors;
                                        }}
                                        enableReinitialize={true}
                                        onSubmit={handleSubmit}
                                    >
                                        {({
                                            values,
                                            errors,
                                            touched,
                                            handleChange,
                                            handleBlur,
                                            handleSubmit,
                                            isSubmitting,
                                        }) => (
                                            <form onSubmit={handleSubmit}>

                                                <div className="row">
                                                {Object.keys(ModuleConfig.formFields).map((field: string, index: number) => {
                                                    const fieldDetails = ModuleConfig.formFields[field]
                                                    return(
                                                        <div key={`field_${index}`} className={fieldDetails.colWidth}>
                                                            <FormField 
                                                                name={field}
                                                                fieldDetails={fieldDetails} 
                                                                values={values}
                                                                errors={errors}
                                                                touched={touched}
                                                                handleChange={handleChange}
                                                                handleBlur={handleBlur}
                                                            />
                                                        </div>
                                                    )
                                                })}
                                                

                                                </div>
                                                <hr />
                                                <button type="submit" className="btn btn-primary" disabled={status === "loading"}>
                                                    Submit
                                                </button>
                                            </form>)}
                                    </Formik>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default SpellCreate;