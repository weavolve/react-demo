import { useEffect } from "react";
import { Alert, Button, Col, Row, Spinner, } from "react-bootstrap"
import { Link } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../../stores/hooks";
import { addToFavouriteList, getSpellListAsync, removeFromFavouriteList, SpellState } from "../SpellSlice";
import { FaRegStar, FaStar, FaEye} from "react-icons/fa";
import { SpellDetailInterface } from "../models/SpellDetailInterface";
import ModuleConfig from "./../config/ModuleConfig";
/**
 * Spelling list page
 */
const SpellList = () => {

    const { spells, status, favourites } = useAppSelector(SpellState);
    const dispatch = useAppDispatch();
    useEffect(() => {
        dispatch(getSpellListAsync());
    }, [dispatch])
    return (
        <>

            <div className="page-header">
                <div className="page-block">
                    <div className="row align-items-center">
                        <div className="col-md-12">
                            <div className="page-header-title">
                                <h5 className="m-b-10">Spell</h5>
                            </div>
                            <ul className="breadcrumb">
                                <li className="breadcrumb-item"><Link to="/"><i className="feather icon-home"></i></Link></li>
                                <li className="breadcrumb-item"><a >Spell List</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div className="main-body">
                <div className="page-wrapper">
                    <div className="row">
                        <div className="col-xl-12">
                            <div className="card">
                                <div className="card-header">
                                    <Row>
                                        <Col auto><h5>Spell List</h5></Col>
                                        <Col md={2}><Link className="btn btn-primary" to="/spell/create">Add New Spell</Link></Col>
                                    </Row>
                                    
                                </div>
                                <div className="card-block table-border-style">
                                    {status === "loading" ?
                                        <Spinner animation="border" role="status">
                                            <span className="visually-hidden">Loading...</span>
                                        </Spinner>
                                        : status === "failed" ?
                                            <Alert key={"danger"} variant={"danger"}>
                                                Somthing went wrong please try again
                                            </Alert>
                                            :
                                            <div className="table-responsive">
                                                <table className="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            {ModuleConfig.listColumnOrder.map((column: string, index: number) => {
                                                                return <th key={`head_${index}`}>{ModuleConfig.formFields[column].label}</th>

                                                            })}
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        {spells?.length >= 1 ? spells.map((spell: SpellDetailInterface, index: number) => {

                                                            const isInFavList = favourites.filter((spl) => spl.index === spell.index).length;
                                                            return (
                                                                <tr key={`table_row_${index}`}>
                                                                    <>
                                                                        {ModuleConfig.listColumnOrder.map((column: string, colIndex: number) => {
                                                                            return (<td key={`table_row_${index}_${colIndex}`}>{spell[column]}</td>)
                                                                        })}

                                                                        <td>
                                                                            <Link to={`/spell/update/${spell[ModuleConfig.primaryKey]}`}><FaEye /></Link>
                                                                            {isInFavList ?
                                                                                <Button variant="link" onClick={() => dispatch(removeFromFavouriteList(spell.index))}>
                                                                                    <FaStar />
                                                                                </Button> :
                                                                                <Button variant="link" onClick={() => dispatch(addToFavouriteList(spell))}>
                                                                                    <FaRegStar />
                                                                                </Button>}
                                                                        </td>
                                                                    </>
                                                                </tr>
                                                            )
                                                        }) : <tr>
                                                            <td colSpan={2}>No Record.</td>
                                                        </tr>}

                                                    </tbody>
                                                </table>
                                            </div>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default SpellList;