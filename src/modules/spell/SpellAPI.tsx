import AxiosInstance from "../../utils/AxiosInstance";
import { SpellDetailInterface } from "./models/SpellDetailInterface";

const API_ROUTE = "spells"
export const getSpellList = async () => {
    return await AxiosInstance.get(`/${API_ROUTE}`)
}

export const getSpellDetail = async (index : string) => {
  return await AxiosInstance.get(`/${API_ROUTE}/${index}`)
}

export const createSpell = async (data : SpellDetailInterface) => {
  return await AxiosInstance.post(`/${API_ROUTE}`, data)
}

export const deleteSpell = async (index : string) => {
  return await AxiosInstance.delete(`/${API_ROUTE}/${index}`)
}
export const updateSpell = async (index : string, data : SpellDetailInterface) => {
  return await AxiosInstance.post(`/${API_ROUTE}/${index}`, data)
}