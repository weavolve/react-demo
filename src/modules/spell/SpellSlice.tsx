import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { RootState } from '../../stores';
import { SpellDetailInterface } from './models/SpellDetailInterface';
import { createSpell, deleteSpell, getSpellDetail, getSpellList, updateSpell } from './SpellAPI';

export interface SpellStateInterface {
  spells: SpellDetailInterface[];
  favourites: SpellDetailInterface[];
  totalRecord: number;
  status: 'idle' | 'loading' | 'failed';
  detail: SpellDetailInterface
}

const initialState: SpellStateInterface = {
  spells: [],
  favourites: [],
  totalRecord: 0,
  status: 'idle',
  detail: {
    index: "",
    name: "",
    url: "",
    desc: [""]
  }
};

export const getSpellListAsync = createAsyncThunk(
  'spell/list',
  async () => {
    const response = await getSpellList();
    return response.data;
  }
);
export const getSpellDetailAsync = createAsyncThunk(
  'spell/detail',
  async (index: string) => {
    const response = await getSpellDetail(index);
    return response.data;
  }
);

export const createSpellAsync = createAsyncThunk(
  'spell/create',
  async (data: SpellDetailInterface) => {
    const response = await createSpell(data);
    return response.data;
  }
);

export const deleteSpellAsync = createAsyncThunk(
  'spell/delete',
  async (index: string) => {
    const response = await deleteSpell(index);
    return response.data;
  }
);

export const updateSpellAsync = createAsyncThunk(
  'spell/update',
  async (data: {
    index: string, data: SpellDetailInterface
  }) => {
    const response = await updateSpell(data.index, data.data);
    return response.data;
  }
);

export const spellSlice = createSlice({
  name: 'spell',
  initialState,
  reducers: {
    addToFavouriteList: (state, action) => {
      state.favourites = [...state.favourites, { ...action.payload }]
    },
    removeFromFavouriteList: (state, action) => {
      const favourites = state.favourites.filter(spell => spell.index !== action.payload);
      state.favourites = favourites
    },
  },
  extraReducers: (builder) => {
    builder
      // GET LIST
      .addCase(getSpellListAsync.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(getSpellListAsync.fulfilled, (state, action) => {
        state.status = 'idle';
        state.spells = action.payload.results;
        state.totalRecord = action.payload.count
      })
      .addCase(getSpellListAsync.rejected, (state) => {
        state.status = 'failed';
      })
      // GET DETAIL
      .addCase(getSpellDetailAsync.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(getSpellDetailAsync.fulfilled, (state, action) => {
        state.status = 'idle';
        state.detail = action.payload;
      })
      .addCase(getSpellDetailAsync.rejected, (state) => {
        state.status = 'failed';
      })
      // CREATE
      .addCase(createSpellAsync.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(createSpellAsync.fulfilled, (state, action) => {
        state.status = 'idle';
        state.detail = action.payload;
      })
      .addCase(createSpellAsync.rejected, (state) => {
        state.status = 'failed';
      })
      // UPDATE
      .addCase(updateSpellAsync.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(updateSpellAsync.fulfilled, (state, action) => {
        state.status = 'idle';
        state.detail = action.payload;
      })
      .addCase(updateSpellAsync.rejected, (state) => {
        state.status = 'failed';
      })
      // DELETE
      .addCase(deleteSpellAsync.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(deleteSpellAsync.fulfilled, (state, action) => {
        state.status = 'idle';
        state.detail = action.payload;
      })
      .addCase(deleteSpellAsync.rejected, (state) => {
        state.status = 'failed';
      });
  },
});

export const { addToFavouriteList, removeFromFavouriteList } = spellSlice.actions;
export const SpellState = (state: RootState) => state.spell;
export default spellSlice.reducer;
