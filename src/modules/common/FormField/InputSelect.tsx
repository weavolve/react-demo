import { useEffect, useState } from 'react';
// import Select from 'react-select';
// import AsyncSelect from 'react-select/async';
import AxiosInstance from '../../../utils/AxiosInstance';

export interface OptionInterface {
    value: string;
    label: string;
}
const InputSelect = (props: any) => {
    const [options, setOptions] = useState<OptionInterface[]>([]);

    useEffect(() => {
        if (!props.fieldDetails.asynchLoad) {
            setOptions(props.fieldDetails.options)
        } else {
            promiseOptions()
        }
    }, []);

    const promiseOptions = () => {
        return AxiosInstance.get(`${props.fieldDetails.dataUrl}`).then(res => {
            const opts: OptionInterface[] = [];
            for (let index = 0; index < res.data.data.length; index++) {
                opts.push({
                    value: res.data.data[index][props.fieldDetails.keyValue],
                    label: res.data.data[index][props.fieldDetails.keylabel]
                });
            }
            return setOptions(opts)
        })

    }
    return (
        <select
            onChange={e => props.setFieldValue(props.name, e.target.value)}
            className='form-control'>
            <option value={""}>{props.fieldDetails.label}</option>
            {options.map((option: any) => {
                return <option selected={option.value === props.values[props.name]} value={option.value}>{option.label}</option>
            })}
        </select>
    )

    // if (props.fieldDetails.asynchLoad) {
    //     return(<AsyncSelect
    //         value={props.values[props.name]}
    //         name={props.name}
    //         id={props.name}
    //         className="form-control p-0"
    //         placeholder={props.fieldDetails.label}
    //         cacheOptions defaultOptions
    //         onChange={(e) => {
    //             props.setFieldValue(props.name, e)
    //         }}
    //         loadOptions={promiseOptions} />)
    // }
    // return (
    //     // <Select
    //     //     value={props.values[props.name]}
    //     //     name={props.name}
    //     //     id={props.name}
    //     //     className="form-control p-0"
    //     //     options={options}
    //     //     onChange={(e) => {
    //     //         props.setFieldValue(props.name, e)
    //     //     }}
    //     //     placeholder={props.fieldDetails.label} />
    // )
}

export default InputSelect;