import DatePicker from "./DatePicker"
import InputSelect from "./InputSelect"
import TextAreaInput from "./TextAreaInput"
import TextInput from "./TextInput"

const FormField = (props: {
    name: string,
    fieldDetails: any,
    values: any,
    errors: any,
    touched: any,
    handleChange: any,
    handleBlur: any,
    setFieldValue?: any
}) => {

    const getField = () => {

        if (props.fieldDetails.inputType === "select") {
            return <InputSelect {...props} />;
        }
        if (props.fieldDetails.inputType === "textarea") {
            return <TextAreaInput {...props} />;
        }
        if (props.fieldDetails.inputType === "datepicker") {
            return <DatePicker {...props} />;
        }
        return <TextInput type={props.fieldDetails.type} {...props} />
    }
    return (
        <div className="form-group mb-4">
            <label htmlFor={props.name}>{props.fieldDetails.label}</label>
            {getField()}
            {typeof props.errors[props.name] === "string" ?
                <div className="error">{props.errors[props.name]}</div>
                : null}
        </div>
    )
}
export default FormField;