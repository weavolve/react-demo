const TextInput = (props: any) => {

    return (
        <input 
            type={props.type}
            name={props.name}
            id={props.name}
            value={props.values[props.name]}
            className="form-control"
            onBlur={props.handleBlur}
            onChange={props.handleChange}
            placeholder={props.fieldDetails.label} />
    )
}

export default TextInput;