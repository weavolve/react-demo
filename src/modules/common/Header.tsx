import { AppState, changeMobileMenuState } from '../../stores/appSlice';
import { useAppDispatch, useAppSelector } from '../../stores/hooks';

const Header = () => {
    const dispatch = useAppDispatch();
    const appState = useAppSelector(AppState);


    return (<header className="navbar pcoded-header navbar-expand-lg navbar-light">
        <div className="m-header">
            <a className={appState.isMobileMenuOpen ?  "mobile-menu on" : "mobile-menu"} onClick={() =>dispatch(changeMobileMenuState())}
         id="mobile-collapse1"><span></span></a>
            <a href="index.html" className="b-brand">
                <div className="b-bg">
                    <i className="feather icon-trending-up"></i>
                </div>
                <span className="b-title">Datta Able</span>
            </a>
        </div>
        
     
    </header>)
}

export default Header;