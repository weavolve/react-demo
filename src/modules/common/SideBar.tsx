import { useState } from 'react';
import { Badge } from 'react-bootstrap';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { Link } from 'react-router-dom';
import { AppState } from '../../stores/appSlice';
import { useAppSelector } from '../../stores/hooks';
import { SpellState } from '../spell/SpellSlice';
import MenuItems from './menu.json';
const SideBar = () => {


    const appState = useAppSelector(AppState);

    const [sideBarCollapsed, setSideBarCollapsed] = useState(false);

    return (
        <Navbar className={
            appState.isMobileMenuOpen ? "pcoded-navbar mob-open" :
            sideBarCollapsed ? "pcoded-navbar navbar-collapsed" : "pcoded-navbar"}>
            <div className="navbar-wrapper">

                <div className="navbar-brand header-logo">
                    <Navbar.Brand as={Link} to="/" className='b-brand' >
                        <div className="b-bg">
                            <i className="feather icon-trending-up"></i>
                        </div>
                        <span className="b-title">Project</span>
                    </Navbar.Brand>

                    <a className={sideBarCollapsed ? "mobile-menu on" : "mobile-menu"} id="mobile-collapse" onClick={() => setSideBarCollapsed(!sideBarCollapsed)}><span></span></a>
                </div>
                <div className="navbar-content scroll-div">
                    <ul className="nav pcoded-inner-navbar">
                        {MenuItems.map((menuItem, index) => {
                            return (
                                <li
                                    className="nav-item active">
                                    <Link to={menuItem.link} className="nav-link "><span className="pcoded-micon"><i
                                        className={`feather ${menuItem.icon}`}></i></span><span className="pcoded-mtext">{menuItem.title}</span></Link>
                                </li>
                            )
                        })}


                    </ul>
                </div>
            </div>
        </Navbar>
    )
}

export default SideBar;