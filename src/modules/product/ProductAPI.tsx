import AxiosInstance from "../../utils/AxiosInstance";
import { ProductDetailInterface } from "./models/ProductDetailInterface";

const API_ROUTE = "products"
export const getProductList = async () => {
    return await AxiosInstance.get(`/${API_ROUTE}`)
}

export const getProductDetail = async (index : string) => {
  return await AxiosInstance.get(`/${API_ROUTE}/${index}`)
}

export const createProduct = async (data : ProductDetailInterface) => {
  return await AxiosInstance.post(`/${API_ROUTE}`, data)
}

export const deleteProduct = async (index : string) => {
  return await AxiosInstance.delete(`/${API_ROUTE}/${index}`)
}
export const updateProduct = async (index : string, data : ProductDetailInterface) => {
  return await AxiosInstance.put(`/${API_ROUTE}/${index}`, data)
}