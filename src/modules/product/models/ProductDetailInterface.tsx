export interface ProductDetailInterface {
    id?: string,
    name?: string
    description?: string
    category_id?: string
}