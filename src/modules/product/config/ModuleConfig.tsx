const ModuleConfig = {
    "primaryKey": "id",
    "formDefaultValue": {
        "name": "",
        "description": "",
        "category_id" : "",
        "statue": ""
    },
    "formFields": {
        "name": {
            "label": "Name",
            "isRequired": true,
            "colWidth": "col-md-6",
            "inputType": "text",
            "type": "text"
        },
        "category_id": {
            "label": "Category",
            "isRequired": true,
            "colWidth": "col-md-6",
            "inputType": "select",
            "asynchLoad": true,
            "dataUrl": "/categories",
            "keyValue": "id",
            "keylabel"  : "name",
            "options": [{
                value: "Test",
                label: "Test"
            }],
            "type": "text"
        },
        "statue": {
            "label": "Statue",
            "isRequired": true,
            "colWidth": "col-md-6",
            "inputType": "select",
            "asynchLoad": false,
            "dataUrl": "/categories",
            "options": [{
                value: "Active",
                label: "Active"
            }, {
                value: "In Active",
                label: "In Active"
            }],
            "type": "text"
        },
        "description": {
            "label": "Description",
            "isRequired": true,
            "colWidth": "col-md-12",
            "inputType": "textarea",
            "type": "text"
        }
    },
    "listColumnOrder": [
        "name",
        "description"
    ]
}

export default ModuleConfig