import { useEffect } from "react";
import { Alert, Button, Col, Row, Spinner, } from "react-bootstrap"
import { Link } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../../stores/hooks";
import { getCategoryListAsync,  CategoryState } from "../CategorySlice";
import { FaEdit, FaTrash } from "react-icons/fa";
import { CategoryDetailInterface } from "../models/CategoryDetailInterface";
import ModuleConfig from "./../config/ModuleConfig";
/**
 * Categorying list page
 */
const CategoryList = () => {

    const { categorys, status, favourites } = useAppSelector(CategoryState);
    const dispatch = useAppDispatch();
    useEffect(() => {
        dispatch(getCategoryListAsync());
    }, [dispatch])
    return (
        <>

            <div className="page-header">
                <div className="page-block">
                    <div className="row align-items-center">
                        <div className="col-md-12">
                            <div className="page-header-title">
                                <h5 className="m-b-10">Category</h5>
                            </div>
                            <ul className="breadcrumb">
                                <li className="breadcrumb-item"><Link to="/"><i className="feather icon-home"></i></Link></li>
                                <li className="breadcrumb-item"><a >Category List</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div className="main-body">
                <div className="page-wrapper">
                    <div className="row">
                        <div className="col-xl-12">
                            <div className="card">
                                <div className="card-header">
                                    <Row>
                                        <Col auto><h5>Category List</h5></Col>
                                        <Col md={2}><Link className="btn btn-primary" to="/category/create">Add New Category</Link></Col>
                                    </Row>

                                </div>
                                <div className="card-block table-border-style">
                                    {status === "loading" ?
                                        <Spinner animation="border" role="status">
                                            <span className="visually-hidden">Loading...</span>
                                        </Spinner>
                                        : status === "failed" ?
                                            <Alert key={"danger"} variant={"danger"}>
                                                Somthing went wrong please try again
                                            </Alert>
                                            :
                                            <div className="table-responsive">
                                                <table className="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            {ModuleConfig.listColumnOrder.map((column: string, index: number) => {
                                                                return <th key={`head_${index}`}>{ModuleConfig.formFields[column].label}</th>

                                                            })}
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        {categorys?.length >= 1 ? categorys.map((category: CategoryDetailInterface, index: number) => {
                                                            return (
                                                                <tr key={`table_row_${index}`}>
                                                                    <>
                                                                        {ModuleConfig.listColumnOrder.map((column: string, colIndex: number) => {
                                                                            return (<td key={`table_row_${index}_${colIndex}`}>{category[column]}</td>)
                                                                        })}

                                                                        <td>
                                                                            <Link to={`/category/update/${category[ModuleConfig.primaryKey]}`}><FaEdit /></Link>
                                                                            <Link to={``}><FaTrash /></Link>
                                                                            
                                                                        </td>
                                                                    </>
                                                                </tr>
                                                            )
                                                        }) : <tr>
                                                            <td colSpan={ModuleConfig.listColumnOrder.length + 1}>No Record.</td>
                                                        </tr>}

                                                    </tbody>
                                                </table>
                                            </div>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default CategoryList;