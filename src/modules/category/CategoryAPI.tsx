import AxiosInstance from "../../utils/AxiosInstance";
import { CategoryDetailInterface } from "./models/CategoryDetailInterface";

const API_ROUTE = "categories"
export const getCategoryList = async () => {
    return await AxiosInstance.get(`/${API_ROUTE}`)
}

export const getCategoryDetail = async (index : string) => {
  return await AxiosInstance.get(`/${API_ROUTE}/${index}`)
}

export const createCategory = async (data : CategoryDetailInterface) => {
  return await AxiosInstance.post(`/${API_ROUTE}`, data)
}

export const deleteCategory = async (index : string) => {
  return await AxiosInstance.delete(`/${API_ROUTE}/${index}`)
}
export const updateCategory = async (index : string, data : CategoryDetailInterface) => {
  return await AxiosInstance.put(`/${API_ROUTE}/${index}`, data)
}