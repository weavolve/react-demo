const ModuleConfig = {
    "primaryKey": "id",
    "formDefaultValue" : {
        "name" : ""
    },
    "formFields": {
        "name": {
            "label": "Name",
            "isRequired": true,
            "colWidth": "col-md-6",
            "inputType": "text",
            "type" :"text"
        }
    },
    "listColumnOrder": [
        "name"    
    ]
}

export default ModuleConfig