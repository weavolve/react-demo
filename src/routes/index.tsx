
import {
    Route,
    Routes
} from "react-router-dom";
import SpellRoute from "../modules/spell/SpellRoute";
import Home from "../modules/Home";

import TaskRoute from "../modules/task/TaskRoute";
import CategoryRoute from "../modules/category/CategoryRoute";
import ProductRoute from "../modules/product/ProductRoute";
{/* Import New Entry */ }

/**
 * This will return all routes of the application
 */
const AppRoute = () => {
    return (
        <div className="pcoded-main-container">
            <div className="pcoded-wrapper">
                <div className="pcoded-content">
                    <div className="pcoded-inner-content">
                        <Routes>
                            <Route path="/" element={<Home />} />
                            {SpellRoute}
{TaskRoute}
{CategoryRoute}
{ProductRoute}
{/* Add New Entry */}
                        </Routes>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AppRoute;