import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import appReducer from './appSlice';
import spellReducer from '../modules/spell/SpellSlice';

import taskReducer from "../modules/task/TaskSlice";
import categoryReducer from "../modules/category/CategorySlice";
import productReducer from "../modules/product/ProductSlice";
/* import store */
export const store = configureStore({
  reducer: {
    app: appReducer,
    spell: spellReducer,
task : taskReducer,
category : categoryReducer,
product : productReducer,
// {addStore}
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
