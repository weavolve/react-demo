import { createSlice } from '@reduxjs/toolkit';
import { RootState } from './';


export interface AppStateInterface {
    isMobileMenuOpen: boolean;
}

const initialState: AppStateInterface = {
    isMobileMenuOpen: false
};


export const appSlice = createSlice({
    name: 'app',
    initialState,
    reducers: {
        changeMobileMenuState: (state) => {
            state.isMobileMenuOpen = !state.isMobileMenuOpen
        }
    }
});

export const { changeMobileMenuState } = appSlice.actions;
export const AppState = (state: RootState) => state.app;
export default appSlice.reducer;
